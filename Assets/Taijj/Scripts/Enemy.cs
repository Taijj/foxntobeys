﻿using System.Collections;
using Taijj.Utils;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// Controller class for the Enemy. Similar to the Hero, the object holding Rigidbody,
    /// and Collider is the _graphics object.
    ///
    /// Enemy will move from left to right between the two anchors.
    /// It will also switch direction if it walks into a wall.
    /// </summary>
    public class Enemy : MonoBehaviour
    {
        #region Init
        public enum Direction
        {
            Left = -1,
            Right = 1
        }

        [SerializeField] private GameObject _graphics;
        [SerializeField] private Animator _animator;
        [SerializeField] private CharacterGrounding _grounding;
        [Space]
        [SerializeField] private float _movementSpeed;
        [SerializeField] private Transform _leftAnchor;
        [SerializeField] private Transform _rightAnchor;
        [SerializeField] private Direction _initialDirection;
        [Space]
        [SerializeField] private int _health;
        [SerializeField] private int _respawnSeconds;
        [SerializeField] private AnimationClip _respawnClip;



        public void Awake()
        {
            Body = _graphics.GetComponent<Rigidbody2D>();
            Collider = _graphics.GetComponent<BoxCollider2D>();
            ColliderBridge = _graphics.GetComponent<ColliderBridge>();

            ColliderBridge.OnCollisionEntered += OnCollissionEnter;
            ColliderBridge.OnTriggerEntered += OnTriggerEntered;
            CurrentDirection = _initialDirection;

            _grounding.Wake(Collider);
            CurrentHealth = _health;
        }

        private Rigidbody2D Body { get; set; }
        private BoxCollider2D Collider { get; set; }
        private ColliderBridge ColliderBridge { get; set; }
        #endregion



        #region Movement
        public void FixedUpdate()
        {
            if (IsDead) return;

            _grounding.TryUpdateGrounded();
            DoMovement();
        }

        private void DoMovement()
        {
            if(_graphics.transform.position.x < _leftAnchor.position.x ||
                _graphics.transform.position.x > _rightAnchor.position.x)
            {
                SwitchDirection();
            }

            float xMovement = (int)CurrentDirection*_movementSpeed;
            Body.velocity = Body.velocity.CloneAndSetX(xMovement);
        }



        public void OnCollissionEnter(Collision2D collision)
        {
            if (IsDead || collision.gameObject.TryGetComponent(out Contact contact) == false)
            {
                return;
            }

            if (contact.IsWall) SwitchDirection();
            if (contact.IsFloor) _grounding.HitGround();
        }

        private void SwitchDirection()
        {
            switch(CurrentDirection)
            {
                case Direction.Left: CurrentDirection = Direction.Right; break;
                case Direction.Right: CurrentDirection = Direction.Left; break;
            }
        }
        #endregion



        #region Death/Respawn Cycle;
        private const string WALK_TRIGGER = "Walk";
        private const string DIE_TRIGGER = "Die";
        private const string RESPAWN_TRIGGER = "Respawn";
        private const string FLASH_TRIGGER = "Flash";

        public void Update() => AlreadyTriggeredThisFrame = false;

        public void OnTriggerEntered(Collider2D collider)
        {
            // This prevents double triggering.
            if (AlreadyTriggeredThisFrame) return;
            AlreadyTriggeredThisFrame = true;

            if (IsDead || collider.gameObject.TryGetComponent(out Contact contact) == false)
            {
                return;
            }

            if (contact.IsDamageSource)
            {
                TakeDamage(collider.transform.position.x > _graphics.transform.position.x ? 1 : -1);
            }
        }

        private void TakeDamage(int lookDirection)
        {
            CurrentHealth--;
            if (IsDead)
            {
                CurrentDirection = (Direction)lookDirection;
                Die();
            }
            else
            {
                _animator.SetTrigger(FLASH_TRIGGER);
            }
        }

        private void Die()
        {
            Body.isKinematic = true;
            Body.simulated = false;
            Collider.enabled = false;

            _animator.SetTrigger(DIE_TRIGGER);
            StartCoroutine(Respawn());
        }

        private IEnumerator Respawn()
        {
            yield return new WaitForSeconds(_respawnSeconds);

            CurrentDirection = _initialDirection;
            _graphics.transform.localPosition = Vector3.zero;
            _animator.SetTrigger(RESPAWN_TRIGGER);
            yield return new WaitForSeconds(_respawnClip.length);

            CurrentHealth = _health;

            Body.isKinematic = false;
            Body.simulated = true;
            Collider.enabled = true;

            _animator.SetTrigger(WALK_TRIGGER);
        }

        private Direction _currentDirection;
        private Direction CurrentDirection
        {
            get => _currentDirection;
            set
            {
                _currentDirection = value;
                _animator.transform.localScale = Vector3.one.CloneAndSetX(-(int)_currentDirection);
            }
        }

        private int CurrentHealth { get; set; }
        private bool IsDead => CurrentHealth <= 0;

        private bool AlreadyTriggeredThisFrame { get; set; }
        #endregion
    }

}