﻿using Taijj.Utils;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    [RequireComponent(typeof(Camera))] // Not necessary but clarifies intention.
    public class FollowCamera : MonoBehaviour
    {
        private const float DIVIDER = 100f; // To allow for more human readable numbers in Inspector.
        [SerializeField] private float _hardness;
        [SerializeField] private Transform _target;
        [SerializeField] private Vector2 _offset;
        [Space]
        [SerializeField] private FloatRange _horizontalLimits;
        [SerializeField] private FloatRange _verticalLimits;

        public void LateUpdate()
        {
            Vector3 newPosition = new Vector3(
                NumberUtils.Clamp(_target.position.x + _offset.x, _horizontalLimits.min, _horizontalLimits.max),
                NumberUtils.Clamp(_target.position.y + _offset.y, _verticalLimits.min, _verticalLimits.max),
                transform.position.z);
            transform.position = Vector3.Lerp(transform.position, newPosition, _hardness/DIVIDER);
        }
    }
}