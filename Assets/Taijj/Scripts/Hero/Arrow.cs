﻿using System;
using Taijj.Utils;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    public class Arrow : MonoBehaviour
    {
        #region Init
        [SerializeField] private Transform _tipAnchor;

        public void Wake(Vector3 direction, float speed)
        {
            Direction = direction.normalized;
            Speed = speed;

            Body = GetComponent<Rigidbody2D>();
            InitialPositionX = transform.position.x;
        }

        private Rigidbody2D Body { get; set; }
        private Vector3 Direction { get; set; }
        private float Speed { get; set; }
        #endregion



        #region Flight
        public event Action<Vector3> OnHit;

        public void Fly()
        {
            if (gameObject.activeSelf == false) return;

            Body.velocity = Direction * Speed;
            TraveledDistance = Mathf.Abs(transform.position.x - InitialPositionX);
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            gameObject.SetActive(false);

            Bounds otherBounds = collider.bounds;
            float impactX = collider.transform.position.x > transform.position.x
                ? otherBounds.center.x - otherBounds.extents.x
                : otherBounds.center.x + otherBounds.extents.x;
            OnHit?.Invoke(_tipAnchor.position.CloneAndSetX(impactX));
        }

        private float InitialPositionX { get; set; }
        public float TraveledDistance { get; private set; }
        #endregion
    }
}