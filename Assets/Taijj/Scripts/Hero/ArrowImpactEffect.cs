﻿using System.Collections;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    public class ArrowImpactEffect : MonoBehaviour
    {
        public void Start()
        {
            Animator animator = GetComponent<Animator>();
            float lifeTime = animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
            StartCoroutine(LifeRoutine(lifeTime));
        }

        private IEnumerator LifeRoutine(float lifeTime)
        {
            yield return new WaitForSeconds(lifeTime);
            Destroy(gameObject);
        }
    }
}