﻿using System.Collections.Generic;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    public class ArrowShooter : MonoBehaviour
    {
        #region Injections
        [SerializeField] private Arrow _arrowPrefab;
        [SerializeField] private ArrowImpactEffect _impactPrefab;
        [Space]
        [SerializeField] private float _arrowSpeed;
        [Tooltip("In Unity Units")] [SerializeField] private float _maxArrowDistance;
        [SerializeField] private float _heatupRate;
        [SerializeField] private float _overheatThreshhold;
        [Space]
        [SerializeField] private float _impactScatter;
        #endregion



        #region FixedUpdate Based
        public void UpdateArrows()
        {
            for(int i = Arrows.Count-1; i >= 0; i--)
            {
                Arrow arrow = Arrows[i];
                arrow.Fly();
                if (arrow.TraveledDistance >= _maxArrowDistance) DestroyArrow(arrow);
            }
        }

        private List<Arrow> Arrows { get; set; } = new List<Arrow>();
        #endregion



        #region Update Based
        public void Shoot(Transform heroTransform)
        {
            CreateArrow(heroTransform);

            CurrentHeat += _heatupRate;
            if (CurrentHeat >= _overheatThreshhold)
            {
                IsOverheated = true;
                CurrentHeat = _overheatThreshhold;
            }
        }

        public void Update()
        {
            if (CurrentHeat <= 0f)
            {
                CurrentHeat = 0f;
                return;
            }

            CurrentHeat -= Time.deltaTime;
            if (IsOverheated)
            {
                IsOverheated = CurrentHeat > 0f;
            }
        }

        private float CurrentHeat { get; set; }
        public bool IsOverheated { get; private set; }
        #endregion



        #region Arrow Factory
        private void CreateArrow(Transform heroTransform)
        {
            Arrow arrow = Instantiate(_arrowPrefab);
            arrow.transform.SetParent(transform);
            arrow.transform.position = heroTransform.position;
            arrow.transform.localScale = heroTransform.localScale;
            arrow.Wake(Vector3.right * heroTransform.localScale.x, _arrowSpeed);
            arrow.OnHit += OnArrowHit;
            Arrows.Add(arrow);
        }

        private void DestroyArrow(Arrow arrow)
        {
            Arrows.Remove(arrow);
            arrow.OnHit -= OnArrowHit;
            Destroy(arrow.gameObject);
        }

        private void OnArrowHit(Vector3 impactPosition)
        {
            // Will auto destruct itself.
            ArrowImpactEffect impact = Instantiate(_impactPrefab);
            impact.transform.position = new Vector3(
                impactPosition.x + Random.Range(-_impactScatter, _impactScatter),
                impactPosition.y + Random.Range(-_impactScatter, _impactScatter),
                0f);
        }
        #endregion
    }
}