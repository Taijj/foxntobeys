﻿using TMPro;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// I hope you liked them :3
    /// </summary>
    public class EasterEgg : MonoBehaviour
    {
        #region Fields
        private const string SHOW_TRIGGER = "Show";
        private const string HIDE_TRIGGER = "Hide";

        [SerializeField] private GameObject _container;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private string _greeting;
        [SerializeField] private string _farewell;
        private Animator Animator { get; set; }
        #endregion



        #region Behavior
        public void Awake()
        {
            Animator = GetComponent<Animator>();

            // Preload, to prevent stutters.
            _container.SetActive(true);
            _container.SetActive(false);
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            _text.text = _greeting;
            Animator.SetTrigger(SHOW_TRIGGER);
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            _text.text = _farewell;
            Animator.SetTrigger(HIDE_TRIGGER);
        }
        #endregion
    }
}