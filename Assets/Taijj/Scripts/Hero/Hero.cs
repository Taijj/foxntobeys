﻿using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// Logic controller of everything necessary to make the hero work.
    ///
    /// Is not the actual moving object! RigidBody and Collider sit
    /// on the the "_graphics" object!
    /// </summary>
    public class Hero : MonoBehaviour
    {
        #region Init
        [SerializeField] private HeroGraphics _graphics;
        [SerializeField] private HeroInput _input;
        [SerializeField] private CharacterGrounding _grounding;
        [SerializeField] private ArrowShooter _arrowShooter;
        [SerializeField] private HeroCooldowns _cooldowns;
        [Space]
        [SerializeField] private float _movementSpeed;
        [SerializeField] [Range(0.001f, 1f)] private float _groundBreakFactor = 1f;
        [SerializeField] [Range(0.001f, 1f)] private float _airBreakFactor = 1f;
        [Space]
        [Tooltip("Includes Jump from the ground.")]
        [SerializeField] private int _maxJumpCount;
        [SerializeField] private float _jumpForce;
        [Space]
        [SerializeField] private int _health;
        [SerializeField] private float _damageKnockbackForce;

        public void Awake()
        {
            Body = _graphics.GetComponent<Rigidbody2D>();
            ColliderBridge = _graphics.GetComponent<ColliderBridge>();

            Collider = _graphics.GetComponent<BoxCollider2D>();
            _grounding.Wake(Collider);
            JumpsLeft = _maxJumpCount;

            Respawn();
        }

        private Rigidbody2D Body { get; set; }
        private BoxCollider2D Collider { get; set; }
        private ColliderBridge ColliderBridge { get; set; }
        private int JumpsLeft { get; set; }
        #endregion



        #region FixedUpdate Based
        private float _lastMovementX;

        public void FixedUpdate()
        {   _arrowShooter.UpdateArrows();
            _grounding.TryUpdateGrounded();
            UpdateMovement();
            _input.ResetButtonPresses();
        }

        private void UpdateMovement()
        {
            if (IsStunned || IsDead) return;

            float xMovement = GetMovementX();
            float yMovement = GetMovementY();

            if (_input.IsIdle)
            {
                xMovement = _lastMovementX * (_grounding.Grounded ? _groundBreakFactor : _airBreakFactor);
            }

            Body.velocity = new Vector2(xMovement, yMovement);
            _lastMovementX = xMovement;
        }

        private float GetMovementX()
        {
            if (_cooldowns.ShootInputCoolsDown) return 0f;

            float result = 0f;
            if (_input.LeftIsHeld)
            {
                result -= _movementSpeed;
            }
            if (_input.RightIsHeld)
            {
                result += _movementSpeed;
            }

            return result;
        }

        private float GetMovementY()
        {
            if (_cooldowns.ShootInputCoolsDown) return 0f;

            float result = Body.velocity.y;
            if (_input.JumpWasPressed && JumpsLeft > 0)
            {
                result = _jumpForce;
                _grounding.Grounded = false;
                _graphics.ResetJump = true;
                JumpsLeft--;
            }
            return result;
        }
        #endregion



        #region Update Based
        public void Update()
        {
            if (IsDead)
            {
                DeathUpdate();
                return;
            }

            _input.UpdateInput();
            DoAnimations();
            DoShooting();
            LifeStateUpdate();
        }

        private void DoShooting()
        {
            if (!_grounding.Grounded || !_input.ShootWasPressed || IsStunned) return;
            _cooldowns.RestartShootInput();

            if (_arrowShooter.IsOverheated) return;

            if (!_cooldowns.ShootRateCoolsDown)
            {
                _arrowShooter.Shoot(_graphics.transform);
                _cooldowns.RestartShootRate();
            }

            if (!_cooldowns.ShootAnimationCoolsDown)
            {
                _graphics.PlayShoot();
                _cooldowns.RestartShootAnimation();
            }
        }

        private void DoAnimations()
        {
            _graphics.Direction = _input.FacingDirection;
            if (_cooldowns.ShootInputCoolsDown) return;

            if (_grounding.Grounded)
            {
                bool showWalk = !_input.IsIdle && !_input.BothAreHeld && IsStunned == false;
                _graphics.PlayGrounded(showWalk);
            }
            else _graphics.PlayUngrounded(Body.velocity.y);
        }



        public void OnCollision(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(out Contact contact) == false)
            {
                return;
            }

            if (contact.IsFloor)
            {
                _grounding.HitGround();
                JumpsLeft = _maxJumpCount;
            }
        }
        #endregion



        #region Damage
        private enum LifeState
        {
            Alive = 0,
            Stunned = 1,
            Invulnerable = 2,
            Dead = 4
        }

        private LifeState State { get; set; }



        private void OnTriggered(Collider2D collider)
        {
            if (State != LifeState.Alive || collider.TryGetComponent(out Contact contact) == false) return;

            if(contact.IsDamageSource)
            {
                CurrentHealth--;
                if(CurrentHealth > 0) Knockback(collider.transform.position.x);
                else Die();
            }
        }

        private void Knockback(float otherPositionX)
        {
            int direction = otherPositionX > _graphics.transform.position.x ? -1 : 1;
            Vector2 knock = new Vector2(direction*2, 1f).normalized;
            Body.velocity = knock * _damageKnockbackForce;

            _graphics.IsInvulnerable = true;
            _cooldowns.RestartStun();
            State = LifeState.Stunned;
        }

        private void LifeStateUpdate()
        {
            if (State == LifeState.Alive || State == LifeState.Dead) return;

            if (State == LifeState.Stunned && !_cooldowns.StunCoolsDown)
            {
                State = LifeState.Invulnerable;
                _cooldowns.RestartInvulnerability();
                return;
            }

            if(State == LifeState.Invulnerable && !_cooldowns.InvulnerabilityCoolsDown)
            {
                State = LifeState.Alive;
                _graphics.IsInvulnerable = false;
            }
        }

        private void DeathUpdate()
        {
            if (State == LifeState.Dead && !_cooldowns.DeathCoolsDown)
            {
                Respawn();
                return;
            }
        }

        private void Die()
        {
            ColliderBridge.OnCollisionEntered -= OnCollision;
            ColliderBridge.OnTriggerStayed -= OnTriggered;

            Body.simulated = false;
            Body.isKinematic = true;
            Collider.enabled = false;

            State = LifeState.Dead;
            _cooldowns.RestartDeath();

            _graphics.PlayDie();
        }

        private void Respawn()
        {
            ColliderBridge.OnCollisionEntered += OnCollision;
            ColliderBridge.OnTriggerStayed += OnTriggered;
            _graphics.transform.localPosition = Vector3.zero;

            Body.simulated = true;
            Body.isKinematic = false;
            Collider.enabled = true;

            CurrentHealth = _health;
            _graphics.Direction = 1;
            State = LifeState.Alive;

            _graphics.PlayGrounded(false);
        }


        private int CurrentHealth { get; set; }
        private bool IsDead => State == LifeState.Dead;
        private bool IsStunned => State == LifeState.Stunned;
        #endregion
    }
}