﻿using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// Central manager of Hero cooldown times.
    /// </summary>
    public class HeroCooldowns : MonoBehaviour
    {
        #region Main
        [SerializeField] private float _shootRate;
        [SerializeField] private float _shootAnimation;
        [SerializeField] private float _shootInput;
        [Space]
        [SerializeField] private float _stun;
        [SerializeField] private float _invulnerabilty;
        [SerializeField] private float _death;

        public void Update()
        {
            DoShootingCooldowns();
            DoDamageCooldowns();
        }
        #endregion




        #region Shooting
        private void DoShootingCooldowns()
        {
            if (ShootRateCounter > 0) ShootRateCounter -= Time.deltaTime;
            if (ShootAnimationCounter > 0) ShootAnimationCounter -= Time.deltaTime;
            if (ShootInputCounter > 0) ShootInputCounter -= Time.deltaTime;
            RefreshShootFlags();
        }

        private void RefreshShootFlags()
        {
            ShootRateCoolsDown = ShootRateCounter > 0f;
            ShootAnimationCoolsDown = ShootAnimationCounter > 0f;
            ShootInputCoolsDown = ShootInputCounter > 0f;
        }

        private float ShootAnimationCounter { get; set; }
        private float ShootRateCounter { get; set; }
        private float ShootInputCounter { get; set; }



        public void RestartShootRate()
        {
            ShootRateCounter = _shootRate;
            RefreshShootFlags();
        }

        public void RestartShootAnimation()
        {
            ShootAnimationCounter = _shootAnimation;
            RefreshShootFlags();
        }

        public void RestartShootInput()
        {
            ShootInputCounter = _shootInput;
            RefreshShootFlags();

        }

        public bool ShootRateCoolsDown { get; private set; }
        public bool ShootAnimationCoolsDown { get; private set; }
        public bool ShootInputCoolsDown { get; private set; }
        #endregion



        #region Damage
        private void DoDamageCooldowns()
        {
            if (StunCounter > 0) StunCounter -= Time.deltaTime;
            if (InvulnerabilityCounter > 0) InvulnerabilityCounter -= Time.deltaTime;
            if (DeathCounter > 0) DeathCounter -= Time.deltaTime;
            RefreshDamageFlags();
        }

        private void RefreshDamageFlags()
        {
            StunCoolsDown = StunCounter > 0f;
            InvulnerabilityCoolsDown = InvulnerabilityCounter > 0f;
            DeathCoolsDown = DeathCounter > 0f;
        }

        private float StunCounter { get; set; }
        private float InvulnerabilityCounter { get; set; }
        private float DeathCounter { get; set; }



        public void RestartStun()
        {
            StunCounter = _stun;
            RefreshDamageFlags();
        }

        public void RestartInvulnerability()
        {
            InvulnerabilityCounter = _invulnerabilty;
            RefreshDamageFlags();
        }

        public void RestartDeath()
        {
            DeathCounter = _death;
            RefreshDamageFlags();
        }

        public bool StunCoolsDown { get; private set; }
        public bool InvulnerabilityCoolsDown { get; private set; }
        public bool DeathCoolsDown { get; private set; }
        #endregion
    }
}