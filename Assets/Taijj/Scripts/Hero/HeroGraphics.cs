﻿using Taijj.Utils;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// Visual parts of the hero. Controls animations and facing direction.
    /// </summary>
    public class HeroGraphics : MonoBehaviour
    {
        #region Init
        // Used for Animator Triggers and State identification (by name).
        private const string IDLE = "Idle";
        private const string WALK = "Walk";
        private const string JUMP = "Jump";
        private const string FALL = "Fall";
        private const string SHOOT = "Shoot";

        private const string INVULNERABLE = "Invulnerable";
        private const string DIE = "Die";

        [SerializeField] private Animator _animator;
        #endregion



        #region Control
        public void PlayGrounded(bool doWalk)
        {
            if (doWalk) TryPlay(WALK);
            else TryPlay(IDLE);
        }

        public void PlayUngrounded(float yVelocity)
        {
            if (yVelocity > 0f)
            {
                if(ResetJump)
                {
                    Play(JUMP);
                    ResetJump = false;
                    return;
                }
                TryPlay(JUMP);
            }
            if (yVelocity < 0f) TryPlay(FALL);
        }



        public void PlayShoot() => Play(SHOOT);

        public bool ResetJump { private get; set; }

        public int Direction
        {
            set
            {
                if (value == 0) return;
                transform.localScale = Vector3.one.CloneAndSetX(value > 0 ? 1 : -1);
            }
        }



        public void PlayDie() => Play(DIE);
        public bool IsInvulnerable
        {
            set
            {
                if (_animator.GetBool(INVULNERABLE) == value) return;
               _animator.SetBool(INVULNERABLE, value);
            }
        }
        #endregion



        #region Animation Triggering
        private void TryPlay(string trigger)
        {
            if (IsPlaying(trigger)) return;
            Play(trigger);
        }

        private bool IsPlaying(string name) => _animator.GetCurrentAnimatorStateInfo(0).IsName(name);

        private void Play(string trigger)
        {
            foreach (AnimatorControllerParameter parameter in _animator.parameters)
            {
                if (parameter.type != AnimatorControllerParameterType.Trigger)
                {
                    continue;
                }
                _animator.ResetTrigger(parameter.name);
            }
            _animator.SetTrigger(trigger);
            _animator.Play(_animator.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, 0);
            _animator.Update(Time.deltaTime);
        }
        #endregion
    }
}