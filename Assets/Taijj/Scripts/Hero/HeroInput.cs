﻿using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// Wrapper to translate Unity's input stuff to easy to use flags.
    /// </summary>
    public class HeroInput : MonoBehaviour
    {
        #region Main
        [Header("Keyboard")]
        [SerializeField] private KeyCode _leftKey;
        [SerializeField] private KeyCode _rightKey;
        [SerializeField] private KeyCode _jumpKey;
        [SerializeField] private KeyCode _shootKey;
        [Header("Keyboard Alternative")]
        [SerializeField] private KeyCode _leftAlt;
        [SerializeField] private KeyCode _rightAlt;
        [SerializeField] private KeyCode _jumpAlt;
        [SerializeField] private KeyCode _shootAlt;
        [Space]
        [Header("Gamepad")]
        [SerializeField] private float _moveAxisThreshhold;
        [SerializeField] private string _gamepadMoveAxis;
        [SerializeField] private KeyCode _jumpButton;
        [SerializeField] private KeyCode _shootButton;

        public void UpdateInput()
        {
            float axisInput = Input.GetAxis(_gamepadMoveAxis);
            LeftIsHeld = Input.GetKey(_leftKey) || Input.GetKey(_leftAlt) || axisInput < -_moveAxisThreshhold;
            RightIsHeld = Input.GetKey(_rightKey) || Input.GetKey(_rightAlt) || axisInput > _moveAxisThreshhold;

            if (!JumpWasPressed) JumpWasPressed = Input.GetKeyDown(_jumpKey) || Input.GetKeyDown(_jumpAlt) || Input.GetKeyDown(_jumpButton);
            if (!ShootWasPressed) ShootWasPressed = Input.GetKeyDown(_shootKey) || Input.GetKeyDown(_shootAlt) || Input.GetKeyDown(_shootButton);

            int direction = 0;
            if (axisInput > 0f || RightIsHeld) direction++;
            if (axisInput < 0f || LeftIsHeld) direction--;
            FacingDirection = direction;

            if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
        }

        public void ResetButtonPresses()
        {
            JumpWasPressed = false;
            ShootWasPressed = false;
        }
        #endregion



        #region Flags
        public int FacingDirection { get; private set; }

        public bool LeftIsHeld { get; private set; }
        public bool RightIsHeld { get; private set; }
        public bool BothAreHeld => LeftIsHeld && RightIsHeld;
        public bool IsIdle => LeftIsHeld == false && RightIsHeld == false;

        public bool JumpWasPressed { get; private set; }
        public bool ShootWasPressed { get; private set; }
        #endregion
    }
}