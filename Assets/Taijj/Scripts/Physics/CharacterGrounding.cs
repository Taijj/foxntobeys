﻿using System.Linq;
using Taijj.Utils;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// Responsible for keeping the info up to date, if a character or
    /// enemy is still on the ground, i.e. when walking of a cliff.
    /// </summary>
    public class CharacterGrounding : MonoBehaviour
    {
        #region Init
        private const int COLLIDER_COUNT = 5;
        [SerializeField] private float _groundedCheckDistance;

        public void Wake(BoxCollider2D collider)
        {
            Grounded = false;
            Collider = collider;
            CheckColliders = new Collider2D[COLLIDER_COUNT];

            Filter = new ContactFilter2D
            {
                maxDepth = 0f,
                minDepth = 0f
            };
        }
        #endregion



        #region Manual Grounding
        public void TryUpdateGrounded()
        {
            if(Grounded)
            {
                UpdateGrounded();
            }
        }

        public void HitGround() => UpdateGrounded();
        #endregion



        #region Grounded Check
        /// <summary>
        /// Checks if any Floor colliders are inside a box, sitting at the
        /// character's bottom.
        ///
        /// Uses OverlapBox, because Raycasts were not able to detect EdgeColliders!
        /// </summary>
        private void UpdateGrounded()
        {
            Bounds bounds = Collider.bounds;
            Box castBox = new Box(
                bounds.center + Vector3.down * (bounds.extents.y + _groundedCheckDistance / 2f),
                new Vector2(bounds.size.x, _groundedCheckDistance));

            ResetColliders();
            Physics2D.OverlapBox(castBox.Center, castBox.Size, 0f, Filter, CheckColliders);
            Grounded = CheckColliders.Where(c => c != null).Any(CanBeStoodOn);
        }

        private void ResetColliders()
        {
            for(int i = 0; i < CheckColliders.Length; i++)
            {
                CheckColliders[i] = null;
            }
        }

        private bool CanBeStoodOn(Collider2D col)
        {
            return col.TryGetComponent(out Contact identifier) && identifier.IsFloor;
        }
        #endregion



        #region Properties
        public bool Grounded { get; set; }

        private BoxCollider2D Collider { get; set; }
        private Collider2D[] CheckColliders { get; set; }
        private ContactFilter2D Filter { get; set; }
        #endregion
    }
}