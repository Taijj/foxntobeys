﻿using System;
using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// Relay component for Unity collision events.
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    public class ColliderBridge : MonoBehaviour
    {
        public event Action<Collision2D> OnCollisionEntered;
        public event Action<Collider2D> OnTriggerEntered;
        public event Action<Collider2D> OnTriggerStayed;

        public void OnCollisionEnter2D(Collision2D collision) { OnCollisionEntered?.Invoke(collision);}
        public void OnTriggerEnter2D(Collider2D collision) => OnTriggerEntered?.Invoke(collision);
        public void OnTriggerStay2D(Collider2D collision) => OnTriggerStayed?.Invoke(collision);
    }
}