﻿using UnityEngine;

namespace Taijj.FOXnTOBEYS
{
    /// <summary>
    /// This is used to assign a certain game relevant kind
    /// to collision objects.
    ///
    /// Is used to answer questions like: Is the thing that just collided with me the floor?
    /// </summary>
    public class Contact : MonoBehaviour
    {
        public enum Kind
        {
            Floor = 0,
            Wall = 1,
            DamageSource = 2
        }

        [SerializeField] private Kind _kind;

        public bool IsFloor => _kind == Kind.Floor;
        public bool IsWall => _kind == Kind.Wall;
        public bool IsDamageSource => _kind == Kind.DamageSource;
    }
}