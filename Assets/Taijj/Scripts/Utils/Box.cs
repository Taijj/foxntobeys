﻿using UnityEngine;

namespace Taijj.Utils
{
    public class Box
    {
        public Box(Vector2 center, Vector2 size)
        {
            Center = center;
            Size = size;

            Vector2 halfSize = size / 2f;
            float halfX = size.x / 2f;
            float halfY = size.y / 2f;

            TopLeft = center + new Vector2(-halfX, halfY);
            Top = center + Vector2.up*halfY ;
            TopRight = center + halfSize;
            Right = center + Vector2.right * halfX;
            BottomRight = center + new Vector2(halfX, -halfY);
            Bottom = center + Vector2.down * halfY;
            BottomLeft = center - halfSize;
            Left = center + Vector2.left * halfX;
        }

        public void DebugDraw(Color color, float duration = 0f)
        {
            Color frameColor = color;
            Color centerColor = new Color(color.r, color.g, color.b, 0.5f);

            Debug.DrawLine(TopLeft, TopRight, frameColor, duration);
            Debug.DrawLine(TopRight, BottomRight, frameColor, duration);
            Debug.DrawLine(BottomRight, BottomLeft, frameColor, duration);
            Debug.DrawLine(BottomLeft, TopLeft, frameColor, duration);
            Debug.DrawLine(Top, Bottom, centerColor, duration);
            Debug.DrawLine(Left, Right, centerColor, duration);
        }



        public Vector2 TopLeft { get; private set; }
        public Vector2 Top { get; private set; }
        public Vector2 TopRight { get; private set; }
        public Vector2 Right { get; private set; }
        public Vector2 BottomRight { get; private set; }
        public Vector2 Bottom { get; private set; }
        public Vector2 BottomLeft { get; private set; }
        public Vector2 Left { get; private set; }

        public Vector2 Center { get; private set; }
        public Vector2 Size { get; private set; }
    }
}